# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CompTimeCalculator do
  describe '.call' do
    it 'creates a file with the result of the ordering' do
      CompTimeCalculator.call

      result = File.new(path_from_last_file_of_tmp).read
      expect(result).to eq(File.new('spec/support/result.json').read)
    end
  end

  def path_from_last_file_of_tmp
    Dir.glob(File.join('tmp', '*.*')).max do |a, b|
      File.ctime(a) <=> File.ctime(b)
    end
  end
end
