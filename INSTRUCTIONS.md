# Banco de horas ACME

## Introdução
A empresa ACME, visando adaptar-se ao mundo moderno, decidiu adotar uma jornada flexível para seus colaboradores.
Num primeiro momento, pareceu uma excelente ideia pois seus funcionários estavam mais contentes com esse novo formato. No entanto, diversos deles acabaram trabalhando menos do que deveriam pois alguns dias estavam desanimados e depois não se lembravam do quanto deveriam repor e, pela falta de ferramentas, os gestores não tiveram como ajudá-los com isso.
A empresa ACME decidiu, então, por contratar a Xerpa para ajudá-la a lidar com esse problema oferecendo maior visibilidade sobre o estado das horas trabalhadas de cada colaborador.

## Desafio
Para conseguir dar visibilidade aos funcionários da ACME, o sistema Xerpa precisa contabilizar  quanto foi trabalhado em cada dia por um dado funcionário e qual a situação de seu saldo de horas.

Para isso, esse serviço receberá um JSON de input input.json que descreve:

 - Período inicial e final das batidas de ponto
 - Dias que o colaborador trabalha na semana
 - Nome e número do PIS do colaborador
 - Tempo mínimo de trabalho diário em minutos
 - Tempo mínimo de descanso diário em minutos
 - Batidas de ponto do funcionário

Com essas informações o sistema deverá checar:

 - Quanto tempo o funcionário trabalhou cada dia no período
 - Quantos tempo ele trabalhou a mais ou a menos naquele dia
 - O Saldo de horas.

## Informações complementares

O intervalo de um funcionário pode ser superior ao definido em sua jornada sem incorrer em hora extra ou débito de horas

 - Exemplo: A pessoa deveria fazer um intervalo de 60 minutos, caso ela faça 60 minutos ou 90 minutos não há diferença

Para todos os fins, a entrada da pessoa é a primeira batida que a pessoa dá no dia e a saída é a última batida que a pessoa dá nesse mesmo dia
Intervalo é o tempo não trabalhado entre a entrada da pessoa no trabalho e sua saída

 - Exemplo: A pessoa bate ponto num dado dia às 10:00, 13:00, 14:00, 19:00; isso significa que ela entrou às 10:00, foi ao intervalo às 13:00, voltou do intervalo às 14:00 e saiu do trabalho às 19:00

Todo tempo de intervalo não gozado é contado como tempo trabalhado ou hora extra desde que o colaborador tenha feito ao menos 50% da jornada do dia

 - Exemplo: A pessoa deveria fazer 60 minutos de intervalo mas só fez 40 e completou sua jornada diária -> Adiciona 20 minutos ao saldo de horas
 - Exemplo: A pessoa deveria fazer 60 minutos mas só fez 20, no entanto, ela trabalhou 1 hora a menos do que devia; após adicionar os 40 minutos do intervalo e subtrair a 1 hora faltante, o saldo do dia vai para 20 minutos negativos


## Regras do desafio
Não deve ser utilizado nenhum framework para isso (bibliotecas para acesso de arquivo, trabalho com datas, uso de JSON, testes, validações e afins são permitidas)
Deve ser feito em uma das seguintes linguagens: Elixir, Erlang, Clojure, Haskell, JavaScript, Java, Ruby, Python, Scala. Utilize a linguagem que você mais tem domínio.
A saída Deve ser impressa no terminal (CLI) ou escrita em um arquivo e deve seguir o padrão mostrado no exemplo mais abaixo.
A entrada Deve ser feita lendo o arquivo ou o recebendo como STDIO pela CLI.
Exemplos de arquivos:

![alt text](images/example.png "Exemplo de input")

Ao terminar, compartilhe o repo privado no bitbucket com bsilva@xerpa.com.br e tgaritezi@xerpa.com.br para que possamos baixar, rodar e avaliar o desafio.

