# frozen_string_literal: true

require 'spec_helper'

RSpec.describe DayBalanceCalculator do
  describe '#worked_minutes' do
    context 'when interval minutes are equal than the minimum interval' do
      it 'does not sum the exceeded interval time' do
        day_entries = [DateTime.new(2018, 6, 19, 9, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 45, 0o0),
                       DateTime.new(2018, 6, 19, 16, 45, 0o0)]
        calculator = DayBalanceCalculator.new(day_entries,
                                              420,
                                              30)

        expect(calculator.worked_minutes).to eq(420)
      end
    end

    context 'when interval is greater than the minimum interval' do
      it 'does not sum the exceed interval time' do
        day_entries = [DateTime.new(2018, 6, 19, 9, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 55, 0o0),
                       DateTime.new(2018, 6, 19, 16, 55, 0o0)]
        calculator = DayBalanceCalculator.new(day_entries,
                                              420,
                                              30)

        expect(calculator.worked_minutes).to eq(420)
      end
    end

    context 'when interval is less than the minimum interval' do
      it 'sums the interval left time' do
        day_entries = [DateTime.new(2018, 6, 19, 9, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 25, 0o0),
                       DateTime.new(2018, 6, 19, 16, 45, 0o0)]
        calculator = DayBalanceCalculator.new(day_entries,
                                              420,
                                              30)

        expect(calculator.worked_minutes).to eq(420 + 20)
      end

      context 'with less than 50% of the minimum workload not achieved' do
        it 'does not sums the interval left time' do
          day_entries = [DateTime.new(2018, 6, 19, 9, 15, 0o0),
                         DateTime.new(2018, 6, 19, 11, 15, 0o0),
                         DateTime.new(2018, 6, 19, 11, 25, 0o0),
                         DateTime.new(2018, 6, 19, 12, 25, 0o0)]
          calculator = DayBalanceCalculator.new(day_entries,
                                                480,
                                                30)

          expect(calculator.worked_minutes).to eq(180 - 20)
        end
      end
    end
  end

  describe '#interval_minutes' do
    context 'when has only one interval' do
      it 'returns the total minutes' do
        day_entries = [DateTime.new(2018, 6, 19, 9, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 25, 0o0),
                       DateTime.new(2018, 6, 19, 16, 45, 0o0)]
        calculator = DayBalanceCalculator.new(day_entries,
                                              420,
                                              30)

        expect(calculator.interval_minutes).to eq(10)
      end
    end

    context 'when has more than one interval' do
      it 'returns the total sum of intervals' do
        day_entries = [DateTime.new(2018, 6, 19, 9, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 25, 0o0),
                       DateTime.new(2018, 6, 19, 13, 15, 0o0),
                       DateTime.new(2018, 6, 19, 13, 45, 0o0),
                       DateTime.new(2018, 6, 19, 16, 55, 0o0)]
        calculator = DayBalanceCalculator.new(day_entries,
                                              420,
                                              30)

        expect(calculator.interval_minutes).to eq(40)
      end
    end

    context 'when has no interval' do
      it 'returns zero' do
        day_entries = [DateTime.new(2018, 6, 19, 9, 15, 0o0),
                       DateTime.new(2018, 6, 19, 12, 15, 0o0)]
        calculator = DayBalanceCalculator.new(day_entries,
                                              180,
                                              0)

        expect(calculator.interval_minutes).to eq(0)
      end
    end
  end
end
