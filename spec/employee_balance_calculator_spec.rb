# frozen_string_literal: true

require 'spec_helper'

RSpec.describe EmployeeBalanceCalculator do
  describe '#balances_per_day' do
    it 'returns a hash with all balances per day' do
      data = JSON.parse(File.new('spec/support/employee_data.json').read)
      calculator = EmployeeBalanceCalculator
                   .new(Date.new(2018, 4, 10),
                        Date.new(2018, 4, 10),
                        data)

      expect(calculator.balances_per_day)
        .to eq([
                 "2018-04-10": {
                   worked_hours_in_minutes: 304,
                   balance_in_minutes: 4
                 }
               ])
    end

    context 'when employee does not work at that day' do
      it 'should return empty' do
        data = JSON.parse(File.new('spec/support/employee_data.json').read)
        calculator = EmployeeBalanceCalculator
                     .new(Date.new(2018, 4, 14),
                          Date.new(2018, 4, 14),
                          data)

        expect(calculator.balances_per_day).to eq([])
      end
    end
  end

  describe '#total_balance' do
    it 'returns the total balance of all days the employee have worked' do
      data = JSON.parse(File.new('spec/support/employee_data.json').read)
      calculator = EmployeeBalanceCalculator
                   .new(Date.new(2018, 0o4, 10),
                        Date.new(2018, 0o5, 0o7),
                        data)

      expect(calculator.total_balance).to eq(-1194)
    end
  end
end
