# frozen_string_literal: true

class DayBalanceCalculator
  attr_accessor :worked_minutes, :interval_minutes

  def initialize(day_entries, workload, minimum_rest_interval)
    @day_entries = day_entries
    @workload = workload
    @minimum_rest_interval = minimum_rest_interval
    @worked_minutes = 0
    @interval_minutes = 0
    calculate
  end

  private

  attr_accessor :day_entries, :workload, :minimum_rest_interval

  def calculate
    segregate_worked_time_from_interval
    @worked_minutes -= interval_left_time if minimum_workload_not_achieved?
  end

  def segregate_worked_time_from_interval
    time_slots = define_time_slots
    while time_slots.count.positive?
      @worked_minutes += time_slots.shift.to_i
      @interval_minutes += time_slots.shift.to_i
    end
  end

  def define_time_slots
    day_entries.each_with_index.map do |day, index|
      ((day - day_entries[(index - 1)]) * 24 * 60).to_i if index.positive?
    end.compact
  end

  def minimum_workload_not_achieved?
    @worked_minutes < (workload / 2.0) && @worked_minutes.positive?
  end

  def interval_left_time
    minimum_rest_interval - @interval_minutes
  end
end
