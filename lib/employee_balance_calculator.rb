# frozen_string_literal: true

class EmployeeBalanceCalculator
  def initialize(period_start, period_end, employee_data)
    @period_start = period_start
    @period_end = period_end
    @entries = employee_data['entries']
    @workload = employee_data['workload']
    @total_balance = 0
  end

  def total_balance
    balances_per_day
    @total_balance
  end

  def balances_per_day
    @balances_per_day ||=
      (@period_start..@period_end).map do |day|
        next unless should_have_work_this_day?(day)
        day_balance = balance_for(day)
        @total_balance += calculate_balance(day_balance.worked_minutes)
        hash_for(day, day_balance)
      end.compact
  end

  private

  def should_have_work_this_day?(day)
    @workload['days'].include?(day.strftime('%a').downcase)
  end

  def balance_for(day)
    days = entries_for_day(day, @entries)
    DayBalanceCalculator.new(days,
                             @workload['workload_in_minutes'],
                             @workload['minimum_rest_interval_in_minutes'])
  end

  def entries_for_day(day, entries)
    entries_sorted(entries).select { |entry_day| entry_day.to_date == day }
  end

  def entries_sorted(entries)
    entries.map { |e| DateTime.parse(e) }.sort
  end

  def calculate_balance(worked_minutes)
    (worked_minutes - @workload['workload_in_minutes']).to_i
  end

  def hash_for(day, day_balance)
    {
      "#{day.to_s}": {
        worked_hours_in_minutes: day_balance.worked_minutes,
        balance_in_minutes: calculate_balance(day_balance.worked_minutes)
      }
    }
  end
end
