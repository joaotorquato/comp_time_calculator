# frozen_string_literal: true

class CompTimeCalculator
  def self.call
    new.calculate
  end

  def initialize
    @input = JSON.parse(File.new('spec/support/input.json').read)
    @period_start = Date.parse(input['period_start'])
    @period_end = Date.parse(input['today'])
    @file = File.new("tmp/comp_time_#{Time.new}.json", 'w')
  end

  def calculate
    file << JSON.pretty_generate(body)
    file.close
  end

  private

  attr_accessor :file, :input, :period_start, :period_end

  def body
    {
      period_start: input['period_start'],
      period_end: input['today'],
      employees: employees_hash
    }
  end

  def employees_hash
    input['employees'].map do |employee|
      {
        pis_number: employee['pis_number'],
        name: employee['name']
      }.merge(employee_entries(employee))
    end
  end

  def employee_entries(employee)
    employee_balance = EmployeeBalanceCalculator.new(@period_start,
                                                     @period_end,
                                                     employee)
    {
      entries: employee_balance.balances_per_day,
      total_balance_in_minutes: employee_balance.total_balance
    }
  end
end
