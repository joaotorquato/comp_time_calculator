# frozen_string_literal: true

require 'json'
require 'date'
require 'fileutils'

dir = 'tmp/'
Dir.mkdir(dir) unless Dir.exist?(dir)

dir = File.expand_path(__dir__)
autoload :CompTimeCalculator, dir + '/comp_time_calculator'
autoload :DayBalanceCalculator, dir + '/day_balance_calculator'
autoload :EmployeeBalanceCalculator, dir + '/employee_balance_calculator'
