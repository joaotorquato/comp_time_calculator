# Compensatory time challenge

## Instructions

* [Portuguese](INSTRUCTIONS.md)

### Installing dependencies

This project uses:
 -  The 2.4.0 Ruby version.

You can install ruby with [RVM](https://rvm.io/rvm/install) and then install the correct version of Ruby:

        $ rvm install ruby-2.4.0

Install dependencies using bundler like above:

        $ bundle install

In case you don't have bundler:

        $ gem install bundler

#### Automated tests with RSpec

You can run all tests using:

        $ rspec

#### Running in your local machine

You can run like above:

        $ ruby run.rb

And then check the folder /tmp for the last file generated to get the result
